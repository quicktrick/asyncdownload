﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncDownload
{
    class Program
    {
        static string urlTemplate = @"http://gallica.bnf.fr/ark:/12148/bpt6k12005590/f{0}.highres";
        static int startPage = 1;
        static int endPage = 768;
        static string localDir = @"d:\Languages\Dictionaries\DATA\French\Grand Larousse de la langue française\Pages\Vol7\";
        static string localFileTemplate = "page{0:0000}.jpg";

        static void Main(string[] args)
        {
            List<int> pageNumbers = new List<int>();

            for (int i = startPage; i <= endPage; i++)
            {
                pageNumbers.Add(i);
            }

            DownloadImages(pageNumbers);
            Console.ReadKey();
        }

        static async void DownloadImages(IEnumerable<int> enumerable)
        {
            var results = new List<Tuple<string, string, Exception>>();
            await enumerable.ForEachAsync(i => DownloadFileTaskAsync(GetRemotePath(i), GetLocalPath(i), 600000), (n, t) => results.Add(t));
        }

        static string GetRemotePath(int i)
        {
            string remotePath = string.Format(urlTemplate, i);
            return remotePath;
        }

        static string GetLocalPath(int i)
        {
            string fileName = string.Format(localFileTemplate, i);
            string filePath = Path.Combine(localDir, fileName);
            return filePath;
        }

        /// <summary>
        ///     Downloads a file from a specified Internet address.
        /// </summary>
        /// <param name="remotePath">Internet address of the file to download.</param>
        /// <param name="localPath">
        ///     Local file name where to store the content of the download, if null a temporary file name will
        ///     be generated.
        /// </param>
        /// <param name="timeOut">Duration in miliseconds before cancelling the  operation.</param>
        /// <returns>A tuple containing the remote path, the local path and an exception if one occurred.</returns>
        private static async Task<Tuple<string, string, Exception>> DownloadFileTaskAsync(string remotePath,
            string localPath = null, int timeOut = 3000)
        {
            try
            {
                if (remotePath == null)
                {
                    Console.WriteLine("DownloadFileTaskAsync (null remote path): skipping");
                    throw new ArgumentNullException("remotePath");
                }

                if (localPath == null)
                {
                    Console.WriteLine(string.Format("DownloadFileTaskAsync (null local path): generating a temporary file name for {0}", remotePath));
                    localPath = Path.GetTempFileName();
                }

                using (var client = new WebClient())
                {
                    client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko");

                    TimerCallback timerCallback = c =>
                    {
                        var webClient = (WebClient)c;
                        if (!webClient.IsBusy) return;
                        webClient.CancelAsync();
                        Console.WriteLine(string.Format("DownloadFileTaskAsync (time out due): {0}", remotePath));
                    };
                    using (var timer = new Timer(timerCallback, client, timeOut, Timeout.Infinite))
                    {
                        await client.DownloadFileTaskAsync(remotePath, localPath);
                    }
                    Console.WriteLine(string.Format("DownloadFileTaskAsync (downloaded): {0}", remotePath));
                    return new Tuple<string, string, Exception>(remotePath, localPath, null);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<string, string, Exception>(remotePath, null, ex);
            }
        }
    }

    public static class Extensions
    {
        public static Task ForEachAsync<TSource, TResult>(
            this IEnumerable<TSource> source,
            Func<TSource, Task<TResult>> taskSelector, Action<TSource, TResult> resultProcessor)
        {
            var oneAtATime = new SemaphoreSlim(4, 8);
            return Task.WhenAll(
                from item in source
                select ProcessAsync(item, taskSelector, resultProcessor, oneAtATime));
        }

        private static async Task ProcessAsync<TSource, TResult>(
            TSource item,
            Func<TSource, Task<TResult>> taskSelector, Action<TSource, TResult> resultProcessor,
            SemaphoreSlim oneAtATime)
        {
            TResult result = await taskSelector(item);
            await oneAtATime.WaitAsync();
            try
            {
                resultProcessor(item, result);
            }
            finally
            {
                oneAtATime.Release();
            }
        }
    }
}
